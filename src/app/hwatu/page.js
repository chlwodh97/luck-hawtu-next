'use client'

import { useSelector, useDispatch } from 'react-redux'
import { resetCard, mixingCard, pickCard } from "@/lib/features/hwatu/hwatuSlice"
import axios from "axios"
import { useState } from "react"

export default function HwatuPage() {
    const [response, setResponse] = useState('')

    const dispatch = useDispatch()
    const { mixCards, pickCards } = useSelector((state) => state.hwatu)

    const handleMixCards = () => {
        dispatch(mixingCard())
    }

    const handlePickCard = (cardName) => {
        if (pickCards.length < 2) {
            dispatch(pickCard(cardName))
        } else {
            alert('최대 두 장의 카드만 선택할 수 있습니다.')
        }
    }

    const handleReset = () => {
        dispatch(resetCard())
        setResponse('')
    }
    const handleConfirm = () => {
        if (pickCards.length === 2) {
            const means = pickCards.flatMap(card => card.means).join(', ')
            const commend = `${means} 라는 단어를 이용해서 5줄로 알잘딱깔센 같은 Z세대 용어 섞어서 운세를 만들어줘`

            const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=AIzaSyAXA9REfGoDBdXsIeBlaw9J5QrdzJa7zqU'

            const data = {"contents":[{"parts":[{"text": commend}]}]}
            axios.post(apiUrl, data)
                .then(res => {
                    setResponse(res.data.candidates[0].content.parts[0].text.split("**"))
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }

    return (
        <div className="jo-big">
            {mixCards.length === 0 ? (

                <button onClick={handleMixCards} className="jo-btn">카드섞기</button>

            ) : (
                <div className="jo-boss">
                    {/*<h1>선택하세요</h1>*/}
                    {mixCards.map((card) => (
                        <div key={card.name} onClick={() => handlePickCard(card.name)}>
                            {card.selected ? (
                                <div>
                                    <img src={`/images/${card.imgName}`} alt={card.name} />
                                    <p className="jo-p">{card.name}</p>
                                </div>
                            ) : (
                                <div>
                                    <img src={`/images/back.png`} alt="back" />
                                </div>
                            )}
                        </div>
                    ))}
                    {pickCards.length === 2 && (
                        <div className="jo-btn2-boss">
                            <button onClick={handleConfirm} className="jo-btn2">운세보기</button>
                            <button onClick={handleReset} className="jo-btn2">다시하기</button>
                            <div className="res">{response && <p>{response}</p>}</div>
                        </div>
                    )}
                </div>
            )}
        </div>
    )
}