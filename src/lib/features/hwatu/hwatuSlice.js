import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    cards: [
        { name: '1월', imgName: '1.png', means: ['소식', '손님', '남자'] },
        { name: '2월', imgName: '2.png', means: ['님', '여자'] },
        { name: '3월', imgName: '3.png', means: ['외출', '혼란', '만남'] },
        { name: '4월', imgName: '4.png', means: ['싸움', '무관심'] },
        { name: '5월', imgName: '5.png', means: ['결혼', '이성'] },
        { name: '6월', imgName: '6.png', means: ['기쁨', '호감'] },
        { name: '7월', imgName: '7.png', means: ['행운', '돈'] },
        { name: '8월', imgName: '8.png', means: ['어둠', '저녁'] },
        { name: '9월', imgName: '9.png', means: ['술'] },
        { name: '10월', imgName: '10.png', means: ['근심', '풍파', '바람'] },
        { name: '11월', imgName: '11.png', means: ['돈', '복'] },
        { name: '12월', imgName: '12.png', means: ['손님', '눈물'] },
    ],
    mixCards: [],
    pickCards: []
}

const hwatuSlice = createSlice({
    name: 'hwatu',
    initialState,
    reducers: {
        resetCard: (state) => {
            state.mixCards = []
            state.pickCards = []
        },
        mixingCard: (state) => {
            state.mixCards = [...state.cards].map(card => ({...card, selected: false}))
            for (let i = state.mixCards.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [state.mixCards[i], state.mixCards[j]] = [state.mixCards[j], state.mixCards[i]];
            }
        },
        pickCard: (state, action) => {
            const cardIndex = state.mixCards.findIndex(card => card.name === action.payload);
            const cardName = state.mixCards.name

            if (cardIndex !== -1 && !state.mixCards[cardIndex].selected) {
                    state.mixCards[cardIndex].selected = true;
                    state.pickCards.push(state.mixCards[cardIndex]);
            }
        }
    }
})

export const { resetCard, mixingCard, pickCard } = hwatuSlice.actions
export default hwatuSlice.reducer