import {configureStore} from "@reduxjs/toolkit";
import hwatuReducer from "./features/hwatu/hwatuSlice"


export default configureStore({
    reducer: {
        hwatu: hwatuReducer,
    },
});